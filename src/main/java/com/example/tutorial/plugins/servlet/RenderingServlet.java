package com.example.tutorial.plugins.servlet;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class RenderingServlet extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(RenderingServlet.class);
    private static final String TEMPLATE_PATH = "/templates/page.vm";

    private final TemplateRenderer templateRenderer;
    private final I18nResolver i18nResolver;

    public RenderingServlet(TemplateRenderer templateRenderer, I18nResolver i18nResolver) {
        this.templateRenderer = templateRenderer;
        this.i18nResolver = i18nResolver;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        Map<String, Object> context = Maps.newHashMap();

        resp.setContentType("text/html;charset=utf-8");
        templateRenderer.render(TEMPLATE_PATH, context, resp.getWriter());
    }

}